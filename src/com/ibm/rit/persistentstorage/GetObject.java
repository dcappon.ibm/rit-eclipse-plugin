package com.ibm.rit.persistentstorage;

import java.util.Vector;

import com.ghc.ghTester.expressions.EvalUtils;
import com.ghc.ghTester.expressions.Function;

public class GetObject extends Function{
	private Function m_fname = null;
	private Function m_key = null;
	private Function m_value = null;

	
	public Function create(int size, Vector params) {
		  		   
		   return new GetObject((Function) params.get(0), (Function) params.get(1),  (Function) params.get(2));
		}
	
	public Object evaluate(Object data) {
		   String fname = m_fname.evaluateAsString(data);
		   String key = m_key.evaluateAsString(data);
		   String value = m_value.evaluateAsString(data);
		   
		   String result = StorageEngine.getObject(fname, key, value);
		   return result;
		}
	
	public GetObject () {
	}
	
	
	protected GetObject (Function f1, Function f2, Function f3) {
		m_fname = f1;
		m_key = f2;
		m_value = f3;
	}
	
}
