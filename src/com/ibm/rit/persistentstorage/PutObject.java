package com.ibm.rit.persistentstorage;
import java.util.Vector;

import com.ghc.ghTester.expressions.EvalUtils;
import com.ghc.ghTester.expressions.Function;

public class PutObject extends Function{
	private Function m_fname = null;
	private Function m_key = null;
	private Function m_value = null;
	private Function m_newJSON = null;

	
	public Function create(int size, Vector params) {
		  		   
		   return new PutObject((Function) params.get(0), (Function) params.get(1),  (Function) params.get(2),  (Function) params.get(3));
		}
	
	public Object evaluate(Object data) {
		   String fname = m_fname.evaluateAsString(data);
		   String key = m_key.evaluateAsString(data);
		   String value = m_value.evaluateAsString(data);
		   String newJSON = m_newJSON.evaluateAsString(data);
		   
		   String result = StorageEngine.putObject(fname, key, value, newJSON);
		   return result;
		}
	
	public PutObject () {
	}
	
	
	protected PutObject (Function f1, Function f2, Function f3, Function f4) {
		m_fname = f1;
		m_key = f2;
		m_value = f3;
		m_newJSON = f4;
	}
	

}
