package com.ibm.rit.persistentstorage;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class StorageEngine {
	
	public static String getObject(String fname, String key, String value) {
		String foundObject = "";
		String rawJSON = "";

		rawJSON = readFileToString(fname);
		
		if (rawJSON != "") {
			Object obj = JSONValue.parse(rawJSON);
			JSONArray array = (JSONArray) obj;
			// once you get the array, you may check items like

			for (int x = 0; x < array.size(); x++) {
				JSONObject o = (JSONObject)array.get(x);
			    String s = (String) o.get(key);
			    
			    if (s.equals(value)) {
			    	foundObject = o.toJSONString();
			    	break;
			    }
			}
		}
		//System.out.println(foundObject);
		return foundObject;
	}

	public static String putObject(String fname, String key, String value, String newJSON) {
		String rawJSON = "";
		String result = "Error";
		int objectToRemove = -1;

		rawJSON = readFileToString(fname);
		
		if (rawJSON != "") {
			Object obj = JSONValue.parse(rawJSON);
			JSONArray array = (JSONArray) obj;
		
			// Find the object to replace
			for (int x = 0; x < array.size(); x++) {
				JSONObject o = (JSONObject)array.get(x);
			    String s = (String) o.get(key);
			    
			    if (s.equals(value)) {
			    	objectToRemove = x;
			    	result = "Updated";
			    	break;
			    }
			}
			if (objectToRemove > -1)  {;
				array.remove(objectToRemove);
			}
			else // -1 so not found, therefore added object
				result = "Added";
			
			JSONObject o = (JSONObject) JSONValue.parse(newJSON);
			array.add(o);
			
			System.out.println(array.toString());
			
			try (FileWriter file = new FileWriter(fname)) {
	            file.write(array.toJSONString());
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
					
			
		}
		//System.out.println(foundObject);
		return result;
	
	}
	
	private static String readFileToString(String fname) {
		StringBuilder contentBuilder = new StringBuilder();

		try (Stream<String> stream = Files.lines(Paths.get(fname), StandardCharsets.UTF_8)) {
			stream.forEach(s -> contentBuilder.append(s));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return contentBuilder.toString();

	}

}
