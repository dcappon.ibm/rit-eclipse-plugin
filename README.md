# RIT eclipse plugin

This project is the output of creating an eclipse plugin project to implement a  Rational Integration Tester custom functions


## Additional Information

- Rational Integration Tester documentation https://help.blueproddoc.com/rationaltest/rationalintegrationtester/10.5.0/docs/topics/c_ritref_custom_funcitons_overview.html

- Additional files folder contains screen shots of the steps and a Rational Integration Project to demonstrate the custom function usage
